#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <string.h> 
#include <string>     
#include <net/if_dl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <map>
#include <thread>

using namespace std;

#pragma comment(lib,"lphlpapi.lib")

#define IP_LEN 4
#define MAC_LEN 6

struct Addr{
  u_char MAC_addr[MAC_LEN];
  u_char IP_addr[IP_LEN];
};

struct Session{
  Addr sender;
  Addr attacker;
  Addr target;
};

void getmyMAC(u_char * mac, char * interface);
void getmyIP(uint8_t *ip_addr, char *interface);
void strtoip(uint8_t *dst_addr, char *src_str);
void printIP(const u_char *addr);
void printMAC(const u_char *addr);

struct ARP_spoof{
  u_char broadcast[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  u_char null_addr[6]={0x00,0x00,0x00,0x00,0x00,0x00};    
  Session session;
  ARP_spoof(){
    memset(session.sender.MAC_addr, 0, MAC_LEN);
    memset(session.target.MAC_addr, 0, MAC_LEN);
  }
  void init(Addr attacker, char *src_IP, char *dst_IP);
  void send_ARP_packet(pcap_t* handle, u_char* ether_dst, u_char* ether_src, Addr src, Addr dst, u_short ARP_TYPE);
  bool sender_MAC(map<uint32_t,uint64_t>* MAC_map, pcap_t *handle);
  bool target_MAC(map<uint32_t,uint64_t>* MAC_map, pcap_t *handle);
  void session_MAC(map<uint32_t,uint64_t>* MAC_map, pcap_t* handle);
  void send_spoofing_packet(pcap_t *handle);
  bool chk_recover(u_char *packet);
  bool relay(pcap_t *handle, struct pcap_pkthdr* header, u_char *packet);
  void arp_spoof(pcap_t *handle);
};